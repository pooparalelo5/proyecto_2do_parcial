/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz_Usuario;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Fernando Changoluisa
 */
public class Pantalla_Inicio {
    private BorderPane root;
     //Image fondo = new Image (k.getImagen());
    
    public Pantalla_Inicio(){
        root = new BorderPane();
        root.setCenter(menu_inicial());
        root.setPrefSize(k.getAncho(), k.getAlto());
        root.maxHeight(k.getAncho());
        root.maxWidth(k.getAlto());
        root.setStyle("-fx-background-image: url('"+k.getImagen()+"parque.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+k.getAncho()+" "+k.getAlto()+"; "
                + "-fx-background-position: center center;");
        
        
    }
    
    private VBox menu_inicial (){
        VBox menu = new VBox();
        Button start = new Button ("Start");
        Button scores = new Button ("Puntajes");
        Button salir =  new Button ("Salir");
        menu.getChildren().addAll(start, scores,salir);
        menu.setAlignment(Pos.CENTER);
        menu.setSpacing(20);
        return menu;
        
        
    }
    
    
    public BorderPane getRoot(){
        return this.root;
    }
    
    
            
    
    
            
    
}
